import { Logger,ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder,SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const logger = new Logger('ApplicationDetails',true);
  const app = await NestFactory.create(AppModule,{ cors: true });
  const configService = app.get(ConfigService);// para usar variables de entorno espesificamente la creada con la configuracion de la basede datos
  const port = configService.get('APP_PORT');

  app.setGlobalPrefix('api/insoftar');// para agregar un prefijo global a la app
  app.useGlobalPipes(new ValidationPipe({
      whitelist:true
    })
  );// validar los dto para cada end-poin


  const options = new DocumentBuilder()
    .setTitle('Prueba Insoftar')
    .setDescription('API usuarios para prueba de insoftar')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('swagger', app, document);

  await app.listen(port);
  logger.log(`El servidor está encendido en el puerto ${ await app.getUrl() }`);
}
bootstrap();
