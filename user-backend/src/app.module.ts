import { UserModule } from './modules/user/user.module';
import { DatabaseModule } from './database/database.module';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config'
import databaseConfig from './config/database.config';

@Module({
  imports: [
    UserModule,
    DatabaseModule,
    ConfigModule.forRoot({
      load: [databaseConfig],// cargar la confguracon de la base de datos
      isGlobal: true,
      envFilePath: '.env'
    })
  ],
  controllers: [],
  providers: [],
})
export class AppModule { }
