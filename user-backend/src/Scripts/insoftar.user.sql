CREATE DATABASE IF NOT EXISTS insoftar;

CREATE TABLE IF NOT EXISTS insoftar.users (
    id int(5) NOT NULL AUTO_INCREMENT,
    firstname varchar(150) NOT NULL,
    lastname varchar(150) NOT NULL,
    identitycard int NOT NULL,
    email varchar(100) NOT NULL,
    phone varchar(16) NOT NULL,
    is_active boolean DEFAULT true,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    CONSTRAINT UX_insoftar_email UNIQUE (email),
    CONSTRAINT UX_insoftar_identitycard UNIQUE (identitycard)
    )