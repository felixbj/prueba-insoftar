// Para guardar constantes usadasen la applicacion y evitar los magicstring
export const TYPEORM_CONFIG = 'database.config';

export const NameIsNotEmpty = 'el nombre no debe estar vacío';
export const NameLength = 'el nombre debe tener más de $constraint1 caracteres y no superar los $constraint2';
export const SurnameIsNotEmpty = 'el apellido no debe estar vacío';
export const SurnameLength = 'el apellido debe tener más de $constraint1 caracteres y no superar los $constraint2';
export const IdentitycardIsNotEmpty = 'la cedula no debe estar vacía';
export const IdentitycardLength = 'la cedula debe tener más de $constraint1 numeros y no superar los $constraint2';
export const EmailIsNotEmpty = 'el email no debe estar vacío';
export const EmailIsEmail = 'el email debe ser un correo electrónico valido';
export const PhoneIsNotEmpty = 'el teléfono no debe estar vacío';
export const PhoneLength = 'el teléfono debe tener $constraint1 caracteres con el formato +57 123 123 4567';
export const IsActiveIsNotEmpty = 'el valor activo no debe estar vacío';
export const CreateUserConflictCedula = 'la cedula ya esta registrada';
export const CreateUserConflictEmail = 'el email ya esta registrado';
