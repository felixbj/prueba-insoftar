import { registerAs }from '@nestjs/config';// registrar variable de entoprno en tiempo de ejecucion
import { TypeOrmModuleOptions  } from  '@nestjs/typeorm';
import { join } from 'path';

function typeOrmModuleOptons(): TypeOrmModuleOptions {
    return {
        type: 'mysql',
        host: process.env.DB_HOST,
        port: parseInt(process.env.DB_PORT,10),
        username: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_DATABASE,

        entities: [join(__dirname,'../modules/**/**/*.entity{.ts,.js}')],
        autoLoadEntities: true,
        
        synchronize: false // no sincroniza las entidades con la base de datos
    }
}
export default registerAs('database',() =>({
    config: typeOrmModuleOptons()
}));