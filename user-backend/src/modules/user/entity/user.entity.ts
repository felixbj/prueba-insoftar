import { BaseEntity,Column,Entity,PrimaryGeneratedColumn } from "typeorm";

@Entity('users')// Decorador indica el nombre de la entidad nombre de tabla en l bd
export class User extends BaseEntity{
    @PrimaryGeneratedColumn('increment')
    id: number;
    @Column({ type:'varchar',nullable:false})
    firstname: string;
    @Column({ type:'varchar',nullable:false})
    lastname: string;
    @Column({ type:'int',nullable:false,unique:true })
    identitycard: number;
    @Column({ type:'varchar',nullable:false,unique:true })
    email: string
    @Column({ type:'varchar',nullable:false})
    phone: string;
    @Column({ type:'boolean',name:'is_active',default:true })
    isActive: boolean;
    @Column({ type:'timestamp',name:'created_at',default: () => 'CURRENT_TIMESTAMP' })
    createdAt: Date;
    @Column({ type:'timestamp',name:'updated_at',default: () => 'CURRENT_TIMESTAMP',nullable:true })
    updatedAt: Date;
}