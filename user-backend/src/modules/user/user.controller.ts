import { BadRequestException, Body,ConflictException,Controller,Delete,Get,NotFoundException,Param,ParseIntPipe,Post,Put } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { plainToClass } from 'class-transformer';
import { CreateUserConflictCedula, CreateUserConflictEmail } from 'src/config/constants';
import { CreateUserDto, ReadUserDto, UpdateUserDto } from './dto';
import { User } from './entity';
import { UserService } from './user.service';

@ApiTags('user')
@Controller('user')
export class UserController {
    constructor(
        private readonly userService:UserService
    ){}// constructor para incorporar el servicio
    @Get('paginate/:skip')
    async findPaginate(@Param('skip',ParseIntPipe) skip:number){
        const {data,count} = await this.userService.findPaginate(skip);
        return {
            message: 'Todos los usuarios paginate',
            data: data.map(
                (user:User) => plainToClass(ReadUserDto,user)
            ),
            count
        };
    }
    @Get()
    async getAllUser(){
        const data = await this.userService.getAll();
        return {
            message: 'Todos los usuarios',
            data,
        };
    }
    @Get(':id')
    async getOneUser(@Param('id',ParseIntPipe) idUser:number){
        if (!idUser) throw new BadRequestException();
        if (!await this.userService.idExists(idUser)) throw new NotFoundException()
        const data = await this.userService.getOne(idUser);
        return {
            message: `Un solo usuario con el id ${idUser}`,
            data,
        };
    }
    @Post()
    async createOneUser(@Body() userDto:CreateUserDto){
        if (!userDto) throw new BadRequestException();
        const { identitycard,email } = userDto;
        if(await this.userService.identityExists(String(identitycard))) throw new ConflictException({message:CreateUserConflictCedula,info:'Conflict'});
        if(await this.userService.emailExists(email)) throw new ConflictException({message:CreateUserConflictEmail,info:'Conflict'});
        const data = await this.userService.createOne(userDto);
        return {
            message: `Crea un solo usuario con el nombre ${data.firstname}`,
            data,
        };
    }
    @Put(':id')
    async editOneUser(@Param('id',ParseIntPipe) idUser:number,@Body() userDto:UpdateUserDto){
        if (!idUser) throw new BadRequestException();
        if (!await this.userService.idExists(idUser)) throw new NotFoundException()
        const user = await this.userService.idExists(idUser);
        if (userDto.hasOwnProperty('identitycard'))
            if(String(user.identitycard) !== String(userDto.identitycard))
                if(await this.userService.identityExists(userDto.identitycard)) throw new ConflictException({message:CreateUserConflictCedula,info:'Conflict'});
        if (userDto.hasOwnProperty('email'))
            if(String(user.email) !== String(userDto.email))
                if(await this.userService.emailExists(userDto.email)) throw new ConflictException({message:CreateUserConflictEmail,info:'Conflict'});
        
        const data = await this.userService.editOne(idUser,userDto);
        return {
            message: `Edita un solo usuario con el id ${data.id}`,
            data
        };
    }
    @Delete(':id')
    async markDeleteOne(@Param('id',ParseIntPipe) idUser:number){
        if (!idUser) throw new BadRequestException();
        if (!await this.userService.idExistsNoState(idUser)) throw new NotFoundException()
        const data =  await this.userService.markDeleteOne(idUser);
        return {
            message: `Elimina un solo usuario con el id ${idUser}`,
            data
        };
    }
    @Delete('delete/:id')
    async deleteOneUser(@Param('id',ParseIntPipe) idUser:number){
        if (!idUser) throw new BadRequestException();
        if (!await this.userService.idExistsNoState(idUser)) throw new NotFoundException()
        const data =  await this.userService.deleteOne(idUser);
        return {
            message: `Elimina un solo usuario con el id ${idUser}`,
            data
        };
    }
}
