import { ApiProperty } from "@nestjs/swagger";
import { IsEmail,IsNotEmpty,IsOptional,Length } from "class-validator";
import { EmailIsEmail,EmailIsNotEmpty,IdentitycardIsNotEmpty,IdentitycardLength,IsActiveIsNotEmpty,NameIsNotEmpty,NameLength,PhoneIsNotEmpty,PhoneLength,SurnameIsNotEmpty,SurnameLength } from "src/config/constants";

export class UpdateUserDto {
    @ApiProperty()
    @IsOptional()
    @IsNotEmpty({message:NameIsNotEmpty})
    @Length(3,150,{message:NameLength})
    readonly firstname: string;
    @ApiProperty()
    @IsOptional()
    @IsNotEmpty({message:SurnameIsNotEmpty})
    @Length(3,150,{message:SurnameLength})
    readonly lastname: string;
    @ApiProperty()
    @IsOptional()
    @IsNotEmpty({message:IdentitycardIsNotEmpty})
    @Length(1,10,{message:IdentitycardLength})
    readonly identitycard: string;
    @ApiProperty()
    @IsOptional()
    @IsNotEmpty({message:EmailIsNotEmpty})
    @IsEmail({},{message:EmailIsEmail})
    readonly email: string;
    @ApiProperty()
    @IsOptional()
    @IsNotEmpty({message:PhoneIsNotEmpty})
    @Length(16,16,{message:PhoneLength})
    readonly phone: string;
    @ApiProperty()
    @IsOptional()
    @IsNotEmpty({message:IsActiveIsNotEmpty})
    readonly isActive?: boolean;
}