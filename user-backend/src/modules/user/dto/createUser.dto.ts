import { ApiProperty } from "@nestjs/swagger";
import { IsEmail,IsNotEmpty,Length } from "class-validator";
import { EmailIsEmail,EmailIsNotEmpty,IdentitycardIsNotEmpty,IdentitycardLength,NameIsNotEmpty,NameLength,PhoneIsNotEmpty,PhoneLength,SurnameIsNotEmpty,SurnameLength } from "src/config/constants";

export class CreateUserDto {
    @ApiProperty()
    @IsNotEmpty({message:NameIsNotEmpty})
    @Length(3,150,{message:NameLength})
    readonly firstname: string;
    @ApiProperty()
    @IsNotEmpty({message:SurnameIsNotEmpty})
    @Length(3,150,{message:SurnameLength})
    readonly lastname: string;
    @ApiProperty()
    @IsNotEmpty({message:IdentitycardIsNotEmpty})
    @Length(1,10,{message:IdentitycardLength})
    readonly identitycard: number;
    @ApiProperty()
    @IsNotEmpty({message:EmailIsNotEmpty})
    @IsEmail({},{message:EmailIsEmail})
    readonly email: string;
    @ApiProperty()
    @IsNotEmpty({message:PhoneIsNotEmpty})
    @Length(16,16,{message:PhoneLength})
    readonly phone: string;
}