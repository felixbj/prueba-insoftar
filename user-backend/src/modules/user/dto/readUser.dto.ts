import { Exclude, Expose } from "class-transformer";
import { IsEmail,IsNumber,IsString } from "class-validator";

@Exclude()
export class ReadUserDto {
    @Expose()
    @IsNumber()
    readonly id: number;
    @Expose()
    @IsString()
    readonly firstname: string;
    @Expose()
    @IsString()
    readonly lastname: string;
    @Expose()
    @IsNumber()
    readonly identitycard: string;
    @Expose()
    @IsString()
    @IsEmail()
    readonly email: string;
    @Expose()
    @IsString()
    readonly phone: string;
    @Expose()
    readonly isActive: boolean;
}