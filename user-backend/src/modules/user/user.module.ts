import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from './entity';
import { UserController } from './user.controller';
import { UserService } from './user.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([UserRepository])// repositorio/s que usara user
    ],
    controllers: [
        UserController
    ],
    providers: [
        UserService
    ],
})
export class UserModule { }
