import { BadRequestException, ConflictException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { DeleteResult, UpdateResult } from 'typeorm';
import { CreateUserDto,ReadUserDto,UpdateUserDto } from './dto';
import { User,UserRepository } from './entity';

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(User)// importante incorporar el repo de tpo entdad user
        private readonly userRepository:UserRepository
    ){}// constructor para usar el repostorio typeorm y la entidad user
    async getAll():Promise<ReadUserDto[]>{
        return (await this.userRepository.find({ where:{ isActive:true } })).map(
            (user:User) => plainToClass(ReadUserDto,user)// class-transformer pasar la clase user a un dto
        );
    }
    async getOne(id:number):Promise<ReadUserDto>{
        if (!id) throw new BadRequestException();
        const data = await this.idExists(id);
        if (!data) throw new NotFoundException()
        return plainToClass(ReadUserDto,data);
    }
    async createOne(user:CreateUserDto):Promise<ReadUserDto>{
        if (!user) throw new BadRequestException();
        this.userRepository.create(user);
        try {
            const data = this.userRepository.create(user);
            return plainToClass(ReadUserDto, await this.userRepository.save(data));
        } catch (error) {
            if (error) throw new BadRequestException(error.sqlMessage);
        }
    }
    async editOne(id:number,user:UpdateUserDto):Promise<ReadUserDto>{
        if (!id) throw new BadRequestException();
        if (!user) throw new BadRequestException();
        const data = await this.idExists(id);
        if (!data) throw new NotFoundException()
        const editedData = Object.assign(data,user);
        try {
            return plainToClass(ReadUserDto, await this.userRepository.save(editedData));
        } catch (error) {
            if (error) throw new BadRequestException(error.sqlMessage);
        }
    }
    async markDeleteOne(id:number):Promise<UpdateResult>{
        if (!id) throw new BadRequestException();
        const user = await this.idExistsNoState(id);
        if (!user) throw new NotFoundException()
        if(user.isActive)
            return await this.userRepository.update(id,Object.assign(user,{ isActive: false }));//marca de elimnado
        else
            return await this.userRepository.update(id,Object.assign(user,{ isActive: true }));//quitar marca de elimnado
    }
    async deleteOne(id:number):Promise<DeleteResult>{
        if (!id) throw new BadRequestException();
        const data = await this.userRepository.findOne(id,{ where:{ isActive:false } });// si tiene marca de eliminado
        if (!data) throw new NotFoundException()  
        return await this.userRepository.delete(id);
    }
    /**
     * PAGINATE QUERY
     */
    async findPaginate(skip): Promise<any> {
        const take = 5
        const _skip = skip
        const [result, total] = await this.userRepository.findAndCount(
            {
                take: take,
                skip: _skip
            }
        );
        return {
            data: result,
            count: total
        }
    }
    /**
     * Si existe email
     * @param  string email
     * @return Promise<User>
     */
    async emailExists(email:string):Promise<User>{
        return await this.userRepository.findOne({
            where:[{ email }]
        });
    }
    /**
     * Si existe cedula
     * @param  string  identitycard
     * @return Promise<User>
     */
    async identityExists(identitycard:string):Promise<User>{
        return await this.userRepository.findOne({
            where:[{ identitycard }]
        });
    }
    /**
     * Si existe id
     * @param  int  id
     * @return Promise<User>
     */
    async idExists(id:number):Promise<User>{
        return await this.userRepository.findOne(id,{ where:{ isActive:true } })
    }
    async idExistsNoState(id:number):Promise<User>{
        return await this.userRepository.findOne(id)
    }
 }
