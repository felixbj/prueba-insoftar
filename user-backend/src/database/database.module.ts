import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { TYPEORM_CONFIG } from '../config/constants';// nombre de le variable de netorno registrada en la config de la  DB

@Module({
    imports: [
        TypeOrmModule.forRootAsync({
          inject: [ConfigService],
          useFactory: (configServce: ConfigService) =>
            configServce.get<TypeOrmModuleOptions>(TYPEORM_CONFIG)
        })
    ],
})
export class DatabaseModule {}
