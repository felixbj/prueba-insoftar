import { UserInterface } from './user.interface';

export interface SimpleResInterface{
    message: string;
    data: UserInterface;
}
export interface MultiResInterface{
    message: string;
    data: UserInterface[];
}