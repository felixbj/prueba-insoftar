export interface UserInterface{
    id?: string;
    firstname: string;
    lastname: string;
    identitycard: string;
    email: string;
    phone: string;
    isActive?: boolean;
}