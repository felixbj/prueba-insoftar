import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserInterface } from '../interfaces/user.interface';
import { MultiResInterface,SimpleResInterface } from '../interfaces/res.interface';
import { environment } from '../../environments/environment';
 

@Injectable({
  providedIn: 'root'
})
export class UserService {
  
  constructor(
    private httpClient:HttpClient
  ) { }

  getUsers(): Observable<MultiResInterface>{
    return this.httpClient.get<MultiResInterface>(`${environment.BASE_URL}/user`);
  }
  getUser(id:string): Observable<SimpleResInterface>{
    return this.httpClient.get<SimpleResInterface>(`${environment.BASE_URL}/user/${id}`);
  }
  cretaeUser(user:UserInterface): Observable<SimpleResInterface>{
    return this.httpClient.post<SimpleResInterface>(`${environment.BASE_URL}/user`,user);
  }
  updateUser(id:string,user:UserInterface): Observable<SimpleResInterface>{
    return this.httpClient.put<SimpleResInterface>(`${environment.BASE_URL}/user/${id}`,user);
  }
  deleteUser(id:string){
    return this.httpClient.delete(`${environment.BASE_URL}/user/${id}`);
  }
  deleteUserdelete(id:string){
    return this.httpClient.delete(`${environment.BASE_URL}/user/delete/${id}`);
  }
  /**paginate */
  getUsersPag(skip:string): Observable<any>{
    return this.httpClient.get(`${environment.BASE_URL}/user/paginate/${skip}`);
  }
}
