import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserIniComponent } from './user-ini.component';

describe('UserIniComponent', () => {
  let component: UserIniComponent;
  let fixture: ComponentFixture<UserIniComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserIniComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserIniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
