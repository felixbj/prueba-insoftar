import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor() { }
  links:any = [];

  ngOnInit(): void {
    this.links = document.getElementById('mobileMenu')?.getElementsByTagName('a');
    for (let index = 0; index < this.links.length; index++) {
      this.links[index].addEventListener('click', function() {
        document.getElementById('mobileMenu')?.classList.toggle('hidden')
        document.getElementById('mobileMenu')?.classList.toggle('block')
      });
    }
  }
  btnMenu(){
    document.getElementById('mobileMenu')?.classList.toggle('hidden')
    document.getElementById('mobileMenu')?.classList.toggle('block')
  }

}
