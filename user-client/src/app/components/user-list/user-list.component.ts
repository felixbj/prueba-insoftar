import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { ShareService } from "../../services/share.service";
import { UserInterface } from '../../interfaces/user.interface'

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  constructor(
    private userService:UserService,
    private shareService:ShareService
  ) { }
  users:UserInterface[] = [];
  npages:number[] = [];

  ngOnInit(): void {
    this.getUsers(0)
    this.shareService.currentApprovalStageMessage.subscribe(msg => {;
      if(msg.localeCompare('renew') == 0)
        this.getUsers(0)
    })
  }
  getUsers(skyp:number){
    //this.userService.getUsers()
    this.userService.getUsersPag(String(skyp))
    .subscribe(
      res => {
        this.npages = new Array(Math.ceil(res.count/5));
        this.users = res.data;
      },
      err => console.log(err)
    )
  }
  //npm ng-modal ng-confirmation-dialogo
  deleteUser(id: string){
    this.userService.deleteUser(id)
    .subscribe(
      res => {
        this.getUsers(0)
      },
      err => console.log(err)
    )
  }
  deleteUserdelete(id: string){
    this.userService.deleteUserdelete(id)
    .subscribe(
      res => {
        this.getUsers(0)
      },
      err => console.log(err)
    )
  }
}
