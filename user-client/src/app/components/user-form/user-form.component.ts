import { Component, OnInit } from '@angular/core';
import { UserInterface } from 'src/app/interfaces/user.interface';
import { UserService } from '../../services/user.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ShareService } from "../../services/share.service";
import { invalid } from '@angular/compiler/src/render3/view/util';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  constructor(
    private userService:UserService,
    private shareService:ShareService,
    private activatedRoute:ActivatedRoute,
    private router:Router
  ) { }

  user:UserInterface = {
    firstname: "",
    lastname: "",
    identitycard: "",
    email: "",
    phone: "",
  }
  edit:boolean = false;
  errors:String[] = [];

  ngOnInit(): void {
    this.initForm();
    const params = this.activatedRoute.snapshot.params;
    if( params.hasOwnProperty('id') ){
      this.userService.getUser(params.id)
      .subscribe(
        res => {
          this.user = res.data;
          this.user.identitycard = String(res.data.identitycard);
          this.edit = true;
        },
        err => console.log(err)
      )
    }
  }
  submitUser(){
    this.hiddenErrors()
    this.user.phone = this.user.phone.replace(/(\d{2})(\d{3})(\d{3})(\d{4})/,"+$1 $2 $3 $4")
    var expresion = /^[a-zA-Z0-9_\&\'\*\+\.\-]+(.[a-zA-Z0-9_\&\'\*\+\.\-]+)*@[a-z0-9]+(.[a-z0-9_\.\-]+)*(\.[a-z]{2,4})+$/;
    if(expresion.test(this.user.email)){
      this.userService.cretaeUser(this.user)
      .subscribe(
        res => {
          if(this.router.isActive('/users',true)){
            this.shareService.updateApprovalMessage('renew')//renovar lista de usuarios
            this.user = res.data;
            this.resetUser()
          }else{
            this.router.navigate(['/users'])
          }
        },
        err => {
          this.errors = (Array.isArray(err.error.message))?err.error.message:[err.error.message];
          document.getElementById('errors')?.classList.remove('hidden');
          document.getElementById('errors')?.classList.add('block');
        }
      )
    }else{
      this.errors = ['el email debe ser un correo electrónico valido']
    }
  }
  updateUser(){
    this.hiddenErrors()
    this.user.phone = this.user.phone.replace(/(\d{2})(\d{3})(\d{3})(\d{4})/,"+$1 $2 $3 $4")
    var expresion = /^[a-zA-Z0-9_\&\'\*\+\.\-]+(.[a-zA-Z0-9_\&\'\*\+\.\-]+)*@[a-z0-9]+(.[a-z0-9_\.\-]+)*(\.[a-z]{2,4})+$/;
    if(expresion.test(this.user.email)){
      this.userService.updateUser(this.user.id || '',this.user)
      .subscribe(
        res => {
          this.user = res.data;
          this.router.navigate(['/users'])
        },
        err => {
          this.errors = (Array.isArray(err.error.message))?err.error.message:[err.error.message];
          document.getElementById('errors')?.classList.remove('hidden');
          document.getElementById('errors')?.classList.add('block');
        }
      )
    }else{
      this.errors = ['el email debe ser un correo electrónico valido']
    }
  }
  resetUser(){
    this.user.firstname = '';
    this.user.lastname = '';
    this.user.identitycard = '';
    this.user.email = '';
    this.user.phone = '';
  }
  initForm(){
    this.setInputFilter(document.getElementById("identitycard"),
      function(value:string) {return /^[0-9]*$/i.test(value); });//solo numeros
    this.setInputFilter(document.getElementById("firstname"),
      function(value:string) {return /^[a-zA-ZñÑáéíóúÁÉÍÓÚ0-9 ]*$/i.test(value); });//solo letras y numeros
    this.setInputFilter(document.getElementById("lastname"),
      function(value:string) {return /^[a-zA-ZñÑáéíóúÁÉÍÓÚ0-9 ]*$/i.test(value); });//solo letras y numeros
    this.setInputFilter(document.getElementById("email"),
      function(value:string) {return /^[a-zA-ZñÑáéíóúÁÉÍÓÚ0-9@_&'*+.-]*$/i.test(value); });//caracteres validos email
    this.setInputFilter(document.getElementById("phone"),
      function(value:string) {return /^[0-9]*$/i.test(value); });//solo numeros
    /* Mas */
    this.setInputMask(document.getElementById("phone"),
      function (value:string){return value.replace(/\D/g,'').substring(0, 12)},
      function (value:string){return value.replace(/(\d{2})(\d{3})(\d{3})(\d{4})/,"+$1 $2 $3 $4")}
    );//Telefono Mask  +($1)-$2-$3-$
  }  
  setInputFilter(textbox:any, inputFilter:any) {//Restringe la entrada al input dado
      if(textbox){
          ["input", "keydown", "keyup", "drop"].forEach(function(event) {
              textbox.addEventListener(event, function() {
                if(inputFilter(textbox.value)) {
                  textbox.oldValue = textbox.value 
                }else if(textbox.hasOwnProperty("oldValue")) {
                  textbox.value = textbox.oldValue;
                }
                if(textbox.type === "email"){//comprobar si es un email valido
                    var expresion = /^[a-zA-Z0-9_\&\'\*\+\.\-]+(.[a-zA-Z0-9_\&\'\*\+\.\-]+)*@[a-z0-9]+(.[a-z0-9_\.\-]+)*(\.[a-z]{2,4})+$/;
                    if(expresion.test(textbox.value)){
                      textbox.classList.remove('ng-invalid');
                      textbox.classList.add('ng-valid');
                    }else{
                      textbox.classList.remove('ng-valid');
                      textbox.classList.add('ng-invalid');
                    }

                }
              });
          });
      }
  }
  /* Mask */
  setInputMask(textbox:any,destroyMaskPhone:any,createMaskPhone:any){
    textbox.addEventListener('focusout', function() {//Telefono Mask
      textbox.value = destroyMaskPhone(textbox.value);//destruye mascara
      textbox.value = createMaskPhone(textbox.value);//crea mascara
    });
  }
  /* Events */
  hiddenErrors(){
    document.getElementById('errors')?.classList.remove('block');
    document.getElementById('errors')?.classList.add('hidden');
    this.errors = []
  }
}
