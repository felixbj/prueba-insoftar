import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserFormComponent } from './components/user-form/user-form.component';
import { UserIniComponent } from './components/user-ini/user-ini.component';

const routes: Routes = [
  {
    path:'users',
    component: UserIniComponent,
  },
  {
    path:'users/create',
    component: UserFormComponent
  },
  {
    path:'users/edit/:id',
    component: UserFormComponent
  }
];//rutas de angular

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
